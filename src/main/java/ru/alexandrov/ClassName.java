package ru.alexandrov;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClassName {
    private String fullName;
    private String simpleName;
}
