package ru.alexandrov;

import java.io.IOException;
import java.util.List;

public class ClassFinder {

    public static void main(String[] args) throws IOException {
        if (args[0] == null || args[1] == null) {
            System.out.println("Need arguments");
            return;
        }
        String path = args[0]; //"src/main/resources/classes.txt";
        String pattern = args[1]; //"FoBa";
        MapOfClasses m = new MapOfClasses(path);
        List<String> list = m.searchByPattern(pattern);
        System.out.println("Pattern: " + pattern);
        System.out.println("Result:");
        for (String s : list) {
            System.out.println(s);
        }
    }
}
