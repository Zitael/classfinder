package ru.alexandrov;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@RequiredArgsConstructor
@Accessors(chain = true)
public class NodeBig extends AbstractNode {
    private final char ch;
    private final ClassNameLink classNameLink;

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Big node, ").append(ch).append("(").append(this.getBigNodeList().size()).append(")");

        if (this.getSmallNode() == null) {
            sb.append(", small node = null");
        } else {
            NodeSmall nextSmall = this.getSmallNode();
            int i = 1;
            while (true) {
                sb.append(", small node #").append(i).append(": ").append(nextSmall).append(" ");
                if (nextSmall.getSmallNode() == null) break;
                nextSmall = nextSmall.getSmallNode();
                i++;
            }
        }
        sb.append(classNameLink);
        return sb.toString();
    }
}
