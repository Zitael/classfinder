package ru.alexandrov;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@Accessors(chain = true)
public class ClassNameLink {
    private boolean isLast;
    private ClassName className;
}
