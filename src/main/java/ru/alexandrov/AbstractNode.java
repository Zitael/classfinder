package ru.alexandrov;

import lombok.Data;

import java.util.ArrayList;

@Data
public class AbstractNode {
    private NodeSmall smallNode;
    private ArrayList<NodeBig> bigNodeList = new ArrayList<>();
}
