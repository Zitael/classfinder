package ru.alexandrov;

import org.junit.Assert.*;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class MapOfClassesTest {
    MapOfClasses subj = new MapOfClasses("src/test/resources/classes-test.txt");

    @Test
    public void searchByPattern_FB() {
        List<String> list = subj.searchByPattern("FB");
        assertEquals(2, list.size());
        assertEquals(list.get(0), "c.d.FooBar");
        assertEquals(list.get(1), "a.b.FooBarBaz");
    }

    @Test
    public void searchByPattern_BF() {
        List<String> list = subj.searchByPattern("BF");
        assertEquals(0, list.size());
    }

    @Test
    public void searchByPattern_fbb() {
        List<String> list = subj.searchByPattern("fbb");
        assertEquals("a.b.FooBarBaz", list.get(0));
        assertEquals(1, list.size());
    }

    @Test
    public void searchByPattern_asterisk() {
        List<String> list = subj.searchByPattern("B*rBaz");
        assertEquals("a.b.FooBarBaz", list.get(0));
        assertEquals(1, list.size());
    }

    @Test
    public void searchByPattern_BrBaz() {
        List<String> list = subj.searchByPattern("BrBaz");
        assertEquals(0, list.size());
    }

    @Test
    public void searchByPattern_null() {
        List<String> list = subj.searchByPattern(null);
        assertEquals(0, list.size());
    }

    @Test
    public void searchByPattern_FoBa() {
        List<String> list = subj.searchByPattern("FoBa");
        assertEquals(2, list.size());
        assertEquals(list.get(0), "c.d.FooBar");
        assertEquals(list.get(1), "a.b.FooBarBaz");
    }

    @Test
    public void searchByPattern_FBar() {
        List<String> list = subj.searchByPattern("FBar");
        assertEquals(2, list.size());
        assertEquals(list.get(0), "c.d.FooBar");
        assertEquals(list.get(1), "a.b.FooBarBaz");
    }

    @Test
    public void searchByPattern_fBb() {
        List<String> list = subj.searchByPattern("fBb");
        assertEquals(0, list.size());
    }

    @Test
    public void searchByPattern_space() {
        List<String> list = subj.searchByPattern("FBar ");
        assertEquals(1, list.size());
        assertEquals(list.get(0), "c.d.FooBar");
    }
}